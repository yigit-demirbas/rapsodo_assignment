export interface ComponentData {
  mobileMediaImageUrl: string;
  desktopMediaImageUrl: string;
  mobileMediaClasses: Array<string>;
  desktopMediaClasses: Array<string>;
  backgroundImageUrl: string;
  content: string;
}
