import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ComponentData } from 'src/shared/models/component-data.model';

@Component({
  selector: '[banner]',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  constructor() {}

  @Input() public componentData: ComponentData;

  ngOnInit() {}
}
