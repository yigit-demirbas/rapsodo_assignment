import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './components/login//login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { BannerComponent } from 'src/shared/components/banner/banner.component';

@NgModule({
  declarations: [LoginComponent, ForgotPasswordComponent, BannerComponent],
  entryComponents: [BannerComponent],
  imports: [CommonModule, LoginRoutingModule, ReactiveFormsModule],
})
export class LoginModule {}
