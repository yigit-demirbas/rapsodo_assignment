import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserDataModel } from '../models/UserDataModel';
import { ApiHelper } from '../helpers/api.helper';
import { Observable } from 'rxjs';
import { LoginResponseModel } from '../models/LoginResponseModel';
import { RegisterResponseModel } from '../models/RegisterResponseModel';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {
    this.apiPath = ApiHelper.getApiPath();
  }

  private apiPath: string;

  postLogin(userData: UserDataModel): Observable<LoginResponseModel> {
    return this.http.post<LoginResponseModel>(`${this.apiPath}api/login`, {
      email: userData.email,
      password: userData.password,
    });
  }

  postRegister(userData: UserDataModel): Observable<RegisterResponseModel> {
    return this.http.post<RegisterResponseModel>(
      `${this.apiPath}api/register`,
      {
        email: userData.email,
        password: userData.password,
      }
    );
  }
}
