import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { filter, take } from 'rxjs/operators';
import { LoginResponseModel } from '../../models/LoginResponseModel';
import { ComponentData } from 'src/shared/models/component-data.model';
import { RegisterResponseModel } from '../../models/RegisterResponseModel';

@Component({
  selector: '[app-login]',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private loginService: LoginService) {}

  public bannerData: ComponentData = {
    backgroundImageUrl: '/assets/images/Path-4658.png',
    content:
      '<img class="d-block d-md-none" src="/assets/images/RapsodoCoachConnectLogoDarkMobile.svg" /><strong>Diamond sports cloud platform.</strong><br />Please sign-in to your account.',
    desktopMediaClasses: ['d-none d-md-block'],
    desktopMediaImageUrl:
      '/assets/images/RapsodoCoachConnectLogoDarkDesktop.svg',
    mobileMediaClasses: ['d-block d-md-none'],
    mobileMediaImageUrl: '/assets/images/players-bg.png',
  };

  public loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  ngOnInit() {
    // set default value for correct user informations
    this.loginForm.patchValue({
      email: 'eve.holt@reqres.in',
      password: 'cityslicka',
    });
  }

  public login() {
    if (!this.loginForm.invalid) {
      this.loginService
        .postLogin({
          email: this.loginForm.controls['email'].value,
          password: this.loginForm.controls['password'].value,
        })
        .pipe(filter(Boolean), take(1))
        .subscribe((res: LoginResponseModel) => {
          if (res && res.token) {
            localStorage.setItem('token', res.token);
            // redirect
          }
        });
    }
  }

  public register() {
    if (!this.loginForm.invalid) {
      this.loginService
        .postRegister({
          email: this.loginForm.controls['email'].value,
          password: this.loginForm.controls['password'].value,
        })
        .pipe(filter(Boolean), take(1))
        .subscribe((res: RegisterResponseModel) => {
          if (res && res.token) {
            localStorage.setItem('token', res.token);
            // redirect
          }
        });
    }
  }
}
