import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { ComponentData } from 'src/shared/models/component-data.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  constructor(private location: Location) {}

  public forgotPasswordForm = new FormGroup({
    email: new FormControl(''),
  });

  public bannerData: ComponentData = {
    backgroundImageUrl: '/assets/images/Path-4658.png',
    content:
      '<img class="d-block d-md-none" src="/assets/images/RapsodoCoachConnectLogoDarkMobile.svg" /><strong>Diamond sports cloud platform.</strong><br />Please sign-in to your account.',
    desktopMediaClasses: ['d-none d-md-block'],
    desktopMediaImageUrl:
      '/assets/images/RapsodoCoachConnectLogoDarkDesktop.svg',
    mobileMediaClasses: ['d-block d-md-none'],
    mobileMediaImageUrl: '/assets/images/players-bg.png',
  };

  ngOnInit() {}

  public continue() {
    // there was no forgot password endpoint in reqres.in
  }

  public cancel() {
    this.location.back();
  }
}
